var express = require("express");
var React = require("react");
var bodyParser = require('body-parser');
var browserify = require('browserify-middleware');
var reactify = require('reactify');
var Router = require('react-router');
var Route = Router.Route;
var Link = Router.Link;

// # Settings
browserify.settings('transform', ['reactify']);
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

// # Make server side resources like javascript, css available for client side
app.use('/css', express.static('stylesheet/css'));
app.use('/js', express.static('js'));

// # Serve server side js for client bundled with dependencies
app.get('/javascript/Search.js', browserify('./component/search/Search.jsx'));
app.get('/javascript/bootstrap-react.js', browserify('./js/bootstrap-react.js'));
app.get('/javascript/routes.js', browserify('./js/routes.js'));
app.get('/javascript/client.js', browserify('./js/client.js'));

var listSearchResult = [
    {
        id: 0,
        title: 'First Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        id: 1,
        title: 'Second Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        id: 2,
        title: 'third Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }
];

app.post('/api', function(req, res) {
    console.log('json received: ', req.body);

    var resListSearchResult = listSearchResult.filter(function (article) {
        return article.title.indexOf(req.body.text) !== -1;
    });

    res.json({
        listSearchResult: resListSearchResult
    });
});

var Router = require('react-router');

var routesFn = require('./js/routes'),
    routes = routesFn(React, Router);

app.get('/*', function(req, res) {
    Router.run(routes, req.path, function (Root, state) {
        var data = {};

        var htmlBody = React.renderToStaticMarkup(<Root data={data} />);

        var html = '<html>';
            html +='    <head>';
            html +='        <title>Experiment</title>';
            html +='        <link rel="stylesheet" href="/css/main.css"/>';
            html +='    </head>';
            html +='    <body>';
            html +='        ' + htmlBody;
            html +='    </body>';
            html +='</html>';

        res.send(html);
    });
});

app.listen(3000);