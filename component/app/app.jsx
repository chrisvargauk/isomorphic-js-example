var React = require('react');

var AppFn = function (RouteHandler, Link) {
    var App = React.createClass({
        render () {
            return (
                <div className= "page">
                    <header>
                        <h1>Experiment</h1>
                    </header>
                    <section className="content">
                        <Link to="search">Search</Link>
                        <hr/>
                        <RouteHandler/>
                    </section>
                    <footer></footer>
                    <script src="/javascript/client.js"></script>
                </div>
            )
        }
    });

    return App;
};

module.exports = AppFn;