"use strict";

var React = require('react');

var AppFn = function AppFn(RouteHandler, Link) {
    var App = React.createClass({
        displayName: "App",

        render: function render() {
            return React.createElement(
                "div",
                { className: "page" },
                React.createElement(
                    "header",
                    null,
                    React.createElement(
                        "h1",
                        null,
                        "Experiment"
                    )
                ),
                React.createElement(
                    "section",
                    { className: "content" },
                    React.createElement(
                        Link,
                        { to: "search" },
                        "Search"
                    ),
                    React.createElement("hr", null),
                    React.createElement(RouteHandler, null)
                ),
                React.createElement("footer", null),
                React.createElement("script", { src: "/javascript/client.js" })
            );
        }
    });

    return App;
};

module.exports = AppFn;
