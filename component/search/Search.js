'use strict';

var React = require('react'),
    reqwest = require('reqwest');

var SearchFn = function SearchFn(Router) {
    var Route = Router.Route;
    var Link = Router.Link;
    var listSearchResult = [{
        id: 0,
        title: 'First Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }, {
        id: 1,
        title: 'Second Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }, {
        id: 2,
        title: 'third Article',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }];

    var Search = React.createClass({
        displayName: 'Search',

        contextTypes: {
            router: React.PropTypes.func
        },

        getInitialState: function getInitialState() {
            console.dir('this.props.param', this.props);

            if (this.props.params.text) {
                var resListSearchResult = listSearchResult.filter((function (article) {
                    return article.title.indexOf(this.props.params.text) !== -1;
                }).bind(this));
            } else {
                resListSearchResult = listSearchResult;
            }

            return {
                searchtext: '... :]',
                listSearchResult: resListSearchResult
            };
        },

        update: function update(e) {
            this.setState({ searchtext: e.target.value });
        },

        sendSearch: function sendSearch() {
            var self = this;

            console.log('sendSearchNew..');
            this.context.router.transitionTo('searchnew', { text: self.state.searchtext });

            reqwest({
                url: '/api',
                method: 'post',
                data: {
                    text: self.state.searchtext
                },
                success: function success(resp) {
                    console.log('resp', resp);
                    self.setState({ listSearchResult: resp.listSearchResult });
                }
            });
        },

        render: function render() {
            return React.createElement(
                'div',
                { className: 'search-bar' },
                React.createElement(
                    'div',
                    null,
                    React.createElement('input', { type: 'text', onChange: this.update }),
                    React.createElement(
                        'button',
                        { onClick: this.sendSearch },
                        'Search'
                    )
                ),
                React.createElement(
                    'ul',
                    { className: 'search-result-wrapper' },
                    this.state.listSearchResult.map(function (searchResult) {
                        return React.createElement(
                            'li',
                            null,
                            React.createElement(
                                Link,
                                { to: 'article', params: { id: searchResult.id } },
                                React.createElement(
                                    'h3',
                                    null,
                                    searchResult.title
                                ),
                                React.createElement(
                                    'div',
                                    null,
                                    searchResult.description
                                )
                            )
                        );
                    })
                )
            );
        }
    });

    return Search;
};

module.exports = SearchFn;
