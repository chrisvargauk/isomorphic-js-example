babel ./component/search/Search.jsx -o ./component/search/Search.js
babel ./component/item/Item.jsx -o ./component/item/Item.js
babel ./component/html-frame/html-frame.jsx -o ./component/html-frame/html-frame.js
babel ./component/article/article.jsx -o ./component/article/article.js
babel ./component/app/app.jsx -o ./component/app/app.js
babel ./server.jsx -o ./server.js
babel js --out-dir js