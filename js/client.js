'use strict';

var React = require("react");
var Router = require('react-router');

var routesFn = require('../js/routes'),
    routes = routesFn(React, Router);

Router.run(routes, Router.HistoryLocation, function (Root) {
    //React.render(<Root/>, document.querySelector('html'));
    var data = {};
    React.render(React.createElement(Root, { data: data }), document.body);
});