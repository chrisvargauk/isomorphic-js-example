// Load React Components
var AppFn = require('../component/app/app'),
    SearchFn = require('../component/search/Search'),
    article = require('../component/article/article');

var routesFn = function (React, Router) {
    var Route = Router.Route,
        RouteHandler = Router.RouteHandler,
        Redirect = Router.Redirect,
        Link = Router.Link,
        App = AppFn(RouteHandler, Link),
        Search = SearchFn(Router);

    var About = React.createClass({
        render () {
            return (
                <div>
                    <h3>About</h3>
                </div>
            )
        }
    });

    var Inbox = React.createClass({
        render () {
            return (
                <div>
                    <h3>Inbox</h3>
                </div>
            )
        }
    });

    var routes = (
        <Route handler={App}>
            <Route name="about" path="about" handler={About}/>
            <Route name="inbox" path="inbox" handler={Inbox}/>
            <Route name="search" path="search" handler={Search}/>
            <Route name="searchnew" path="search/:text" handler={Search}/>
            <Route name="article" path="article/:id" handler={article}/>
        </Route>
    );

    return routes;
};

module.exports = routesFn;