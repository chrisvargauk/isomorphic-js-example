'use strict';

var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var Search = React.createFactory(require('../component/search/Search'));
var article = require('../component/article/article');

var dataHydrated = { initialCount: 8 };

var containerSearch = document.getElementById('react-search-mount');
var htmlFrameFn = require('../component/html-frame/html-frame');
//React.render(new Search(dataHydrated), containerSearch);

var RouteHandler = Router.RouteHandler;

var htmlFrame = htmlFrameFn(RouteHandler);

var routes = React.createElement(Route, { name: "Home", path: "/", handler: htmlFrame }, React.createElement(Route, { name: "Search", path: "search", handler: Search }), React.createElement(Route, { name: "Article", path: "article", handler: article }));

//Router.run(routes, Router.HistoryLocation, function (Handler, routerState) {
//    React.render(React.createElement(Handler, { data: data, loadingEvents: loadingEvents }), element);
//});

Router.run(routes, '/search', function (Handler, routerState) {
    var data = {};

    //React.render(React.createElement(Handler, { data: data, loadingEvents: loadingEvents }), element);
    React.render(React.createElement(Handler, { data: data }), element);
    //var containerSearch = document.getElementById('react-search-mount');
    //React.render(new Search(dataHydrated), containerSearch);
});