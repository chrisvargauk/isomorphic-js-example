"use strict";

var cache = {};

var ensureTokenKey = function ensureTokenKey(token) {
  if (!cache[token]) cache[token] = {};
};

exports.set = function (token, key, data) {
  ensureTokenKey(token);
  cache[token][key] = data;
};

exports.get = function (token, key) {
  ensureTokenKey(token);
  return cache[token][key];
};

exports.clean = function (token) {
  var data = cache[token];
  delete cache[token];
  return data;
};

exports.expire = function (token, key) {
  ensureTokenKey(token);
  delete cache[token][key];
};