// Load React Components
'use strict';

var AppFn = require('../component/app/app'),
    SearchFn = require('../component/search/Search'),
    article = require('../component/article/article');

var routesFn = function routesFn(React, Router) {
    var Route = Router.Route,
        RouteHandler = Router.RouteHandler,
        Redirect = Router.Redirect,
        Link = Router.Link,
        App = AppFn(RouteHandler, Link),
        Search = SearchFn(Router);

    var About = React.createClass({
        displayName: 'About',

        render: function render() {
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'h3',
                    null,
                    'About'
                )
            );
        }
    });

    var Inbox = React.createClass({
        displayName: 'Inbox',

        render: function render() {
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'h3',
                    null,
                    'Inbox'
                )
            );
        }
    });

    var routes = React.createElement(
        Route,
        { handler: App },
        React.createElement(Route, { name: 'about', path: 'about', handler: About }),
        React.createElement(Route, { name: 'inbox', path: 'inbox', handler: Inbox }),
        React.createElement(Route, { name: 'search', path: 'search', handler: Search }),
        React.createElement(Route, { name: 'searchnew', path: 'search/:text', handler: Search }),
        React.createElement(Route, { name: 'article', path: 'article/:id', handler: article })
    );

    return routes;
};

module.exports = routesFn;